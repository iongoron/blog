<?php
require_once "DB.php";
    
class CommentsModel extends DB {
    
     function getAll($search = "", $start = 0, $limit = 5) {
        $sql = 'select * from comments';
        $data = array();
        
        if ($search !== "") {
            $data = ['%' . $search . '%'];
            $sql .= ' where user_name like ?';
        }
        
        $sql .= ' limit ' . $start . ',' . $limit;
        
        return $this->selectAll($sql, $data);
    }
    function selectComment($id) {
        $sql= 'select * from comments where article_id = '. $id;
        return $this->selectAll($sql);
    }
    
    function addComment($item){
        $data = [$item['article_id'],
                $item['user_name'],
                $item['content']];
        $sql = 'insert into comments (article_id, user_name, content) values (?, ?, ?)';
        return $this->insertItem($sql, $data);
    }
    
    function removeComment($item){
        $data = [$item['id']];
        $sql = 'delete from comments where id = ?';
        return $this->deleteItem($sql, $data);
    }
}