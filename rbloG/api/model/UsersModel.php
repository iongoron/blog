<?php

require_once "DB.php";

class UsersModel extends DB {
    function getAll($search = "", $start = 0, $limit = 20) {
        $sql = 'select * from Users';
        $data = array();
        
        if ($search !== "") {
            $data = ['%' . $search . '%'];
            $sql .= ' where email like ?';
        }
        
        $sql .= ' limit ' . $start . ',' . $limit;
        
        return $this->selectAll($sql, $data);
    }
    
    function countUsers($search) {
        $sql = "select id from Users";
        $data = [];
        
        if ($search !== "") {
            $data = ['%' . $search . '%'];
            $sql .= ' where email like ?';
        }
        
        $this->selectAll($sql, $data);
        return $this->countAll();
    }
    
    function addUser($item) {
        $data = [$item['email'],
                    $item['password'],
                    $item['repassword']
                ];

        $sql = 'insert into Users (email, password, repassword) values (?, ?, ?)';
        return $this->insertItem($sql, $data);
    }
     
    
    function deleteUser($item) {
        $data= [$item['id']];
        $sql= 'delete from Users where id = ?';
        return $this->deleteItem($sql, $data);
    }
    function editUsers($item){
        $data = [$item['email'],
                    $item['password'],
                    $item['repassword'],
                    $item['name'],
                    $item['age'],
                    $item['gender'],
                    $item['id']];
                    
        $sql = 'update Users set email = ? , password = ?, repassword=?, name = ? , age = ?, gender = ? where id = ?';
        return $this->updateItem($sql, $data);
    }
    
    function loginUser($item) {
        $data = [
                 $item['email'],
                 $item['password']
                ];
        $sql = 'select * from Users where email = ? and password = ?  limit 1';
        return $this->getItem($sql, $data);
                
    } 
}