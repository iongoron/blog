<?php

define("DB_HOST", 'localhost');
define("DB_NAME", 'BlogDB');
define("DB_USER", 'radugordan');
define("DB_PASS", '');

class DB {
    protected $dbh;
    protected $sth;
    
    function __construct() {
        try {
           $this->dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME , DB_USER, DB_PASS);
        //   echo 'Connection with success!';
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
        }    
    } 
    
    protected function selectAll($sql, $data = array()) {
        //echo $sql;
        // var_dump($data); die;
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data);
        return $this->sth->fetchAll(PDO::FETCH_ASSOC);    
    }
    protected function searchAll($sql, $data ) {
        //echo $sql;
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data);
        return $this->sth->fetchAll(PDO::FETCH_ASSOC);    
    }
    
    protected function countAll() {
        return $this->sth->rowCount();    
    }
    
    protected function getItem($sql, $data){
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data);
        return $this->sth->fetch(PDO::FETCH_ASSOC);
        
        // return $sth->rowCount();
    }
    protected function selectItem($sql){
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute();
        return $this->sth->fetch(PDO::FETCH_ASSOC);
        
        // return $sth->rowCount();
    }
    
    protected function insertItem($sql, $data) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data);
        return $this->dbh->lastInsertId(); 
    }
    
    protected function deleteItem($sql, $data) {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($data);
        // return $sth->fetch(PDO::FETCH_ASSOC);
        return $sth->rowCount();   
    }
    
    protected function updateItem($sql, $data) {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($data);
        return $sth->rowCount();   
        // return $sth->fetchAll(PDO::FETCH_ASSOC);

    }
}