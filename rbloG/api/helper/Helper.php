<?php
require "controllers/Login.php";

class Helper {
    public static function validate_request_admin() {
        if (isset($_SESSION)){
            if ($_SESSION["is_admin"] == false) {
                http_response_code(401);  
                return ["error"=>"UNAUTHORIZED"];
            } else {
                return ["is_admin"=>$_SESSION['is_admin'], "email" => $_SESSION['email']]; 
            }
        } 
        
    }
    
    public static function validate_request() {
            if (!isset($_SESSION["isLogged"]) || $_SESSION['isLogged'] !== TRUE) {
                http_response_code(401);  
                return array("error"=>"UNAUTHORIZED");
            }else {
                return ["isLogged"=>$_SESSION['isLogged'], "email" => $_SESSION['email']]; 
            }
        }
    }
