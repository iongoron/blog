<?php
require "model/ContactModel.php";

    class Contact {
        private $contact_msg;
        function __construct() {
            $this->contact_msg = new ContactModel();
        }
        
        function getAll() {
            return $this->contact_msg->getAll();
        }
        
        function addMessage() {
            return $this->contact_msg->addContactMessage($_POST);
        }
    }