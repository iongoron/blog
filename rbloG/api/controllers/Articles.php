<?php
require "model/ArticlesModel.php";
require "helper/Helper.php";



class Articles {
    private $articleModel;
    function __construct(){
        $this->articleModel = new ArticlesModel;
    }
    
    function searchAll() { 
        $search = (!empty($_GET["search"])) ? ($_GET["search"]) : "";
        return $this->articleModel->getAll($search);
    }
    
    function getAll(){
        
        $search = (!empty($_GET["search"])) ? ($_GET["search"]) : "";
        $page = (!empty($_GET["page"])) ? $_GET["page"] : 1;
        $ipp = 50;
        $start = $ipp*$page-$ipp;
        $list = $this->articleModel->getAll($search, $start, $ipp ); 
        
        // $count = $this->articleModel->countArticles($search); // var_dump($count); die;
        // $nrPages = ceil($count/$ipp);     //var_dump($nrPages); 
        
        return $list;;
    }
    
    
    function getArticle(){
    
         return $this->articleModel->selectArticle($_GET['id']); // var_dump($_GET); die;
        
    }
    
    
    function addArticle(){
        
        // $response = Helper::validate_request_admin();
        // if ($response['error'] == 'UNAUTHORIZED'){
        //     return $response['error'];
        // } else {
        $tmpPath = $_FILES["file"]["tmp_name"]; 
        
        $filePath= "../uploads/" . $_FILES["file"]["name"]; 
        move_uploaded_file($tmpPath, $filePath);
        
        $_POST["file"] = $_FILES["file"]["name"];
        
        return $this->articleModel->addArticle($_POST);
        // }
    }
    
    
    function deleteArticle(){   
        
        global $DELETE;
        
        $response = Helper::validate_request_admin();
        if($response['error'] == "UNAUTHORIZED"){
            return $response['error'];
        } else {
            return $this->articleModel->deleteArticle($DELETE);    
        }
    }
    
    
    function editArticle(){
        
        global $PUT;
        
        $response = Helper::validate_request_admin();
        if ($response['error'] == "UNAUTHORIZED"){
            return $response['error'];
        } else {
            return $this->articleModel->updateArticle($PUT);
        }
    }
}
