<?php
require "model/UsersModel.php";

class Users {
    private $users;
    
    function __construct(){
        $this->users = new UsersModel();
    }
    function getUsers() {
        
         return $this->users->getAll();
    }
        
        
}