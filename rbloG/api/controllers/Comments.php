<?php
require "model/CommentsModel.php";
// require "helper/Helper.php";

class Comments {
    private $commentsModel;
    function __construct(){
        $this->commentsModel = new CommentsModel;
    }
    
    function getAll(){
        // $response = Helper::validate_request(); 
        // if ($response["error"] == "UNAUTHORIZED"){
            
        //     return $response['error'];
        // } else {
            return $this->commentsModel->getAll();
        // }
    }
    
    function selectComment() {
        return $this->commentsModel->selectComment($_GET['id']);
    }
    function addComment(){
        // $response = Helper::validate_request();
        // if ($response["error"] == "UNAUTHORIZED") {
        //     return $response["error"];
        // } else {
            return $this->commentsModel->addComment($_POST);
        // }
    }
}