<?php
session_start();

$routes["articles"] = array(
                        "class" => "Articles",
                        "method" => "getAll");
$routes["articles/item"] = array(
                        "class" => "Articles",
                        "method" => "getArticle");
$routes["articles/search"] = array(
                        "class" => "Articles",
                        "method" => "searchAll");
$routes["articles/add"] = array(
                        "class" => "Articles",
                        "method" => "addArticle");
$routes["articles/edit"] = array(
                        "class" => "Articles",
                        "method" => "editArticle");
$routes["articles/delete"] = array(
                        "class" => "Articles",
                        "method" => "deleteArticle");

$routes["comments"] = array(
                        "class" => "Comments",
                        "method" => "getAll");
$routes["comments/item"] = array(
                        "class" => "Comments",
                        "method" => "selectComment");
$routes["comments/add"] = array(
                        "class" => "Comments",
                        "method" => "addComment");

$routes["users"] = array(
                        "class"=>"Users",
                        "method"=>"getUsers");
$routes["users/session"] = array(
                        "class"=>"Login",
                        "method"=>"validate_request");
$routes["users/signup"] = array(
                        "class"=>"Login",
                        "method"=>"createAccount");
$routes["users/login"] = array(
                        "class"=>"Login",
                        "method"=>"Login");
$routes["users/logout"] = array(
                        "class"=>"Login",
                        "method"=>"Logout");                        
$routes["users/sessionU"] = array(
                        "class"=>"Login",
                        "method"=>"validateUserSession");   
$routes["users/sessionA"] = array(
                        "class"=>"Login",
                        "method"=>"validateAdminSession");    

$routes["contact"] = array(
                        "class"=>"Contact",
                        "method"=>"getAll");
$routes["contact/add"] = array(
                        "class"=>"Contact",
                        "method"=>"addMessage");


    define ("BLOG_DIR", "/blog/api/");
    $redirectUrl = $_SERVER["REDIRECT_URL"]; 
    $page = str_replace(BLOG_DIR, "", $redirectUrl);
    $page = rtrim($page, "/"); 

    if (array_key_exists($page, $routes)){
        
        $method = $_SERVER["REQUEST_METHOD"];
        switch ($method) {
            case "POST":
                $content = file_get_contents("php://input");
                $data = json_decode($content, true);

                if ($data) {
                    $_POST = $data;
                }
                break; 
            case "PUT":($_PUT); 
                $content = file_get_contents("php://input");
                $PUT = json_decode($content, true);
                break;
            case "DELETE":
                $content = file_get_contents("php://input");
                $DELETE = json_decode($content, true);
                break;
        }
        require "controllers/".$routes[$page]["class"]. ".php";
        $controller = new $routes[$page]["class"];
        $result = $controller->$routes[$page]["method"]();
    }
    else {
        $result = ["error" => "page not found"];
        http_response_code(404);
    }
    
    header("Content-Type: application/json");
    echo json_encode($result);