/* global $*/
/* global Articles*/
$(document).ready(onHtmlLoaded);


// function checkSession(){
//      $.ajax({
//         url:"https://gordanproiect-radugordan.c9users.io/blog/api/users/sessionA",
//         type:"GET",
//         success: function(resp) {
//             if(resp.logged == false)
//             window.location = "login.html";
//             console.log(resp);
//         },
//         error: function(){
//             alert("Article was not added");
//         }
//     });
// }

function onHtmlLoaded() {
    var articlesContainer = $("#articles");
    var articles = new Articles();
    //this will populate the models property on articles variable
    var articleXHR = articles.getAll();
    articleXHR.done(function(){
        for(var i=0;i<articles.models.length; i++) {
            var articleElem = $("<li atricle-id="+articles.models[i].id+"><p>"+
                                articles.models[i].title + "</li>" );
            var articleImgElem = $("<img>");
            articleImgElem.attr("src","../../uploads/"+articles.models[i].file)
            articlesContainer.append(articleElem);
            articlesContainer.append(articleImgElem);
            articleElem.on("click", onArticleClicked);
        }
    });
}
function onArticleClicked() {
    var articleId = $(this).attr("atricle-id");
    window.location.href = "https://gordanproiect-radugordan.c9users.io/blog/assets/templates/article.html?id="+articleId;
}

// checkSession();
    