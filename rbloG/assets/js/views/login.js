/* global $ */
/* global Users */

$(document).ready(onHtmlLoaded);

function onHtmlLoaded(){
    var loginBtn = $("#login-btn");
        loginBtn.on("click", function(ev){
            ev.preventDefault();
        var userEmail = $("input[name=user_email]").val();
        var userPassword = $("input[name=user_password]").val();
        
        var formData = new FormData();
        formData.append("email", userEmail);
        formData.append("password", userPassword);
        
        var user = new Users({
            user_email: userEmail ,
            user_password: userPassword
        })
        user.login(formData);
        
    })
}

