/* global $*/
/* global Articles*/
/* global Comments*/
/* global Comment*/

$(document).ready(onHtmlLoaded);

function onHtmlLoaded() {
    var articleId = getUrlParam("id");
    
    var articles = new Articles();
    var comments = new Comments();
    var articlesXHR = articles.getAll();
    articlesXHR.done(function(){
        var currentArticle;
        for(var i=0;i<articles.models.length; i++) {
            if (articles.models[i].id === articleId) {
                currentArticle = articles.models[i];
            }
        }
        
        if (currentArticle) {
            //generate an h2 element for article title
            var titleElem = $("<h2></h2>");
            titleElem.html(currentArticle.title);
            
            //generate an article element for article content
            var contentElem = $("<article></article>");
            contentElem.html(currentArticle.content);
            
            var articleImgElem = $("<img>");
            articleImgElem.attr("src","../../uploads/"+currentArticle.file);
            
            //append the elements in container
            var container = $("#content");
            container.append(titleElem);
            container.append(articleImgElem);
            container.append(contentElem);
            
        }

        var commentsXHR = comments.getComments(articleId);
        commentsXHR.done(function(comments){
            var currentComments ;
            for(var i=0;i<comments.length; i++) {
               var commenmtElem = $("#commentBox");
               commenmtElem.append("<div><div class='user-comment'><p>" + comments[i].user_name + "</p>" +
                                    "<p>" + comments[i].content + "</p>" +
                                    "<p>"+ comments[i].creation_date + "</p></div></div>");
            }
        });
            
        var commentForm = $("#addCommentForm");
        commentForm.on("submit",function(ev){
        ev.preventDefault();
        
        var nameInput = $("input[name='user_name']").val();
        var contentInput = $("#commentContent").val();
        
        var formData = new FormData();
        formData.append("user_name",nameInput);
        formData.append("content",contentInput);
        formData.append("article_id", articleId);
        
        var comment = new Comment({
            user_name: nameInput,
            content: contentInput,
            article_id: articleId
        });
        comment.add(formData); 
        window.location.reload();
            
        });
        });
    }



//util function, will return the url param for the provided key
        function getUrlParam(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results==null){
               return null;
            }
            else{
               return results[1] || 0;
            }
        }