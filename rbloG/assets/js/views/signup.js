/* global $*/
/* global User*/

$(document).ready(onHtmlLoaded);

function onHtmlLoaded(){
    var signup = $("#signup");
    var submitBtn=$("#submitButton");
    submitBtn.on("click",function(){
        var emailInput = $("input[name='email']").val();
        var passwordInput= $("input[name='password']").val();
        var repasswordInput= $("input[name='repassword']").val();
        
        var formData = new FormData();
        formData.append ("email", emailInput);
        formData.append ("password", passwordInput);
        formData.append ("repassword", repasswordInput);
        
        
        var user = new User({
            email: emailInput,
            password: passwordInput,
            repassword: repasswordInput
        });
        user.createAccount(formData);
    })
}