/* global $*/
 
function Article(options) {
    this.id = options.id;
    this.title = options.title;
    this.content = options.content;
    this.file = options.file;
    this.created_at = options.created_at;
}

Article.prototype.add = function(formData){
    
    $.ajax({
        url:"https://gordanproiect-radugordan.c9users.io/blog/api/articles/add",
        type:"POST",
        data:formData,
        processData:false,
        contentType: false,
        success: function(resp) {
            console.log(resp);
        },
        error: function(){
            alert("Article was not added");
        }
    });
}