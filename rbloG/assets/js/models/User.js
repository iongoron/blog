/* global $*/

function User(options){
    this.id = options.id;
    this.email = options.email;
    this.password = options.password;
    this.repassword = options.repassword
}

User.prototype.createAccount=function(formData){
     $.ajax({
        url: "https://gordanproiect-radugordan.c9users.io/blog/api/users/signup",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function(resp){
            console.log ("Account created")
        },
        error:function(xhr, status, error){
            alert("Something went wrong with your signup. Please try again!")
        }
    })
}


