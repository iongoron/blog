/*global $*/

function Comment(option) {
    this.id = option.id;
    this.article_id = option.article_id;
    this.user_name = option.user_name;
    this.content = option.content;
    this.creation_date = option.creation_date;
}

Comment.prototype.add = function(formData) {
    console.log(formData);
     $.ajax({
        url:"https://gordanproiect-radugordan.c9users.io/blog/api/comments/add",
        type: "POST",
        data:formData,
        processData: false,
        contentType: false,
        success: function(resp) {
            console.log("Comment added!");
        },
        error: function(xhr, status, error){
            alert("Comment not added"+status);
        }
    })
}
