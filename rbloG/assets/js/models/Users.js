/* global $*/
/* global User*/

function Users(){
    this.models=[];
}

Users.prototype.login=function (userData){
    // var that=this;
    return $.ajax ({
        url: "https://gordanproiect-radugordan.c9users.io/blog/api/users/login",
        type: "POST",
        data: userData,
        processData: false,
        contentType: false,
        success : function (resp) {
            var selectUser = new User(resp);
            // that.models = [];
            // that.models.push(selectUser);
            console.log("You're logged in!");
        },
        error: function(xhr, status, error){
            alert ("Login failed");
        }
    }) 
}