/* global $*/
/* global Article*/
function Articles() {
    this.models = [];
}

Articles.prototype.getAll = function(searchValue, pageValue) {
    var that = this;
    return $.ajax({
        url:"https://ionutgoron-ionutgoron.c9users.io/aBlog/api/articles",
        type:"GET",
        data:{
          search: searchValue,
          page: pageValue
        },
        success:function(resp) {
            for(var i=0; i<resp.length; i++) {
                var article = new Article(resp[i]);
                that.models.push(article);
            }
        },
        error:function(xhr, status, error) {
            alert("Oops!Something went wrongqqq!");
        }
    });
};