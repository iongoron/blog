/* global $ */
/* global formData */
/* global data */

function Article(options) {
    this.id = options.id;
    this.title = options.title;
    this.creator = options.creator;
    this.content = options.content;
    this.created_at = options.created_at;
    this.image = options.image;
}

Article.prototype.add = function(vars){
    //ajax request to save article 
        
    var vars1 = JSON.stringify(vars);
    console.log (vars1);
    
    $.ajax({
    url: "https://ionutgoron-ionutgoron.c9users.io/aBlog/api/articles/add",
    type: "POST",
    data : vars1,
    processData:false,
    contentType: false,
    success: function( resp) {
       console.log("sucks");
       console.log(resp);
    },
   
   error: function( xhr, status, errorThrown ) {
    //   console.warn(jqxhr.responseText)
       alert( "Sorry, there was a problem!" );
       console.log( "Error: " + errorThrown );
       console.log( "Status: " + status );
              console.dir( xhr );
   },
   complete: function( xhr, status ) {
       alert( "The request is complete!" );
   }
});
};