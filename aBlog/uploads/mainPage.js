/* global $ */

/* global Article */
/* global Users */
/* global localStorage*/
/* global onHtmlLoaded*/
// localStorage.setItem('someKey',"myValue");

window.addEventListener("load", function (){
    
// display articles
  onHtmlLoaded(displayThem);
    function displayThem(value) {  
      // create the article container with attributes
        var articleContainer = document.createElement("div");
        articleContainer.setAttribute("class", "articlePrv");
        articleContainer.setAttribute("id", "articleOpen");
      // create the edit and delete btn
        var butnEdit = document.createElement("button");
      //id 
        butnEdit.setAttribute("class", "editBtn");
      //classes 
        var editClass = butnEdit.classList;
        editClass.add("btn-default", "btn", "btnEdit", "hideit");
        
      // //href  
      //   butnEdit.setAttribute("href", "assets/templates/newArticle.php");
      //text
        var editThis = "Edit article";
        var paragr = document.createElement("p");
        paragr.innerHTML = editThis;
        butnEdit.appendChild(paragr);
        
      // create the delete btn
        var butnDelete = document.createElement("button");
      //id 
        butnDelete.setAttribute("class", "deleteBtn");
      //classes 
        var deleteClass = butnDelete.classList;
        deleteClass.add("btn-default");
        deleteClass.add("btn");
        deleteClass.add("btnDelete");       //compress
        deleteClass.add("hideit");
      //href  
        butnDelete.setAttribute("href", "assets/templates/newArticle.php");
      //text
        var deleteThis = "Delete article";
        var paragr1 = document.createElement("p");
        paragr1.innerHTML = deleteThis;
        butnDelete.appendChild(paragr1);
       
      //div for the user buttons  
        var btnContainer = document.createElement("div");
        btnContainer.setAttribute("class", "control");
        btnContainer.appendChild(butnEdit);
        btnContainer.appendChild(butnDelete);
        // articleContainer.appendChild(btnContainer); //del when done
        
      //img 
        var imgContainer = document.createElement("img");
        imgContainer.setAttribute("class", "imgClass");
        imgContainer.setAttribute("src", value['image']);
      //the div containing the img and the user buttons
        var artSide = document.createElement("div");
        artSide.appendChild(imgContainer);
        artSide.appendChild(btnContainer);
        artSide.setAttribute("class", "sideArt");
      
        articleContainer.appendChild(artSide);
      
      //title  
        var titl = document.createElement("h4");
        titl.setAttribute("class","title");
        titl.setAttribute("class","text");
        titl.innerHTML = value['title'];
        articleContainer.appendChild(titl);
      //text
        var tex = document.createElement("p");
        tex.setAttribute("class","text");
        tex.setAttribute("class","article");
        tex.innerHTML = value['content'];
        articleContainer.appendChild(tex);
        
        if (document.getElementById("container")) {
          document.getElementById("container").appendChild(articleContainer);
        }
// done dispaly articles 

    //open the entire article on click   
      tex.addEventListener("click",function () {
        if (document.getElementById("articleId")){
          document.getElementById("container").removeChild(document.getElementById("articleId"));
          console.log("remove");
        } 
        else {
          
          var articlePage = document.createElement("div");
          
          var imgContainer1 = document.createElement("img");
        imgContainer1.setAttribute("class", "imgClass1");
        imgContainer1.setAttribute("src", value['image']);
      //the div containing the img and the user buttons
        var artSid = document.createElement("div");
        artSid.appendChild(imgContainer1);
        
        
          articlePage.setAttribute("class", "articleClass");
          articlePage.setAttribute("id", "articleId");
          var cls = document.createElement("div");
          cls.setAttribute("id", "closeBt");
          cls.innerHTML = " < Close >";
          var bigArtContent = document.createElement("div");
          bigArtContent.setAttribute("class", "bigContent");
          articlePage.appendChild(bigArtContent);
          bigArtContent.innerHTML =  value['content'];
          articlePage.appendChild(cls);
          articlePage.appendChild(artSid);
          console.log("i do this");
          document.getElementById("container").appendChild(articlePage);
          
        }
        cls.addEventListener("click",function() {
          document.getElementById("container"). removeChild(document.getElementById("articleId"));
          
        });    
      });
    }     

 // for the menu toggle
   function toggleDropdown(toggleItem, itemId, forClass) {
    document.getElementById(toggleItem).addEventListener("click", function(el){
        document.getElementById(itemId).classList.toggle(forClass);
        });
    }
    
    toggleDropdown("toggleMenu", "bs-example-navbar-collapse-1", "collapse");   /*main menu*/
    toggleDropdown("loginId", "logId", "hide");  /* login dropdown*/
    if ((passlog==1)&&(emaillog==1)){
      toggleDropdown("login-btn", "loginId", "hide"); /*hide login btn and login bar */
      toggleDropdown("login-btn", "logId", "hide");   /*hide login btn and login bar */
      toggleDropdown("login-btn", "logout-btn", "hide");  /*show logout btn after login*/
    }  
    toggleDropdown("logout-btn", "loginId", "hide");  /*show login btn after logout*/
    var sessionObj;
    var logOutBtnElem = document.getElementById("logout-btn");
    var loginFormElem = document.getElementById("logId");
    var loginBtnElem = document.getElementById("loginId");
    var h1Elem =document.getElementById("welcome-msg");
    var sessionActive = localStorage.getItem("user_session");
    
//check if user is logged in (if we have data for it on localStorage)
    // if (sessionActive) {
    //     displayLoggedInView();
    // }
    

    function toggleUserDisplay() {
        document.getElementById("newArticle").classList.toggle("hide");
        for(var w=0;w<=document.getElementsByClassName("editBtn").length;w++){
          document.getElementsByClassName("editBtn")[w].classList.toggle("hideit");
          document.getElementsByClassName("deleteBtn")[w].classList.toggle("hideit");
        }
    }

    function displayLoggedInView(){     
       //get session data from local storage
        var session = localStorage.getItem("user_session");
        //parse the resulted string back to a JS Object
        sessionObj = JSON.parse(session);
        h1Elem.innerHTML = "Welcome, "+sessionObj.username;
        loginFormElem.classList.add("hide");
        logOutBtnElem.classList.remove("hide");
        loginBtnElem.classList.add("hide"); /* to avoid both login and logout to appear at refresh wjile log */
        toggleUserDisplay();
        
    }
    
    /*  log out and clear username and pass from local storage
        display login button instead of logout  */
    logOutBtnElem.addEventListener("click",function(){
        localStorage.removeItem("user_session");
        logOutBtnElem.classList.remove("show");
        logOutBtnElem.classList.add("hide");
        toggleUserDisplay();
        h1Elem.innerHTML = "You just logged out...";
    });
    
/*save the user name and pass in local storage     */
    var loginBtn = document.getElementById("login-btn");
        loginBtn.addEventListener("click",function(){
          if ((passlog==1)&&(emaillog==1)){
            var userName = document.getElementById("user_email").value;
            var userPass = document.getElementById("user_password").value;
            document.getElementById("user_email").value = "";
            document.getElementById("user_password").value = "";
            
            var userData = {
                username:userName,
                password:userPass
            };
            var userDataStr = JSON.stringify(userData);
            localStorage.setItem("user_session",userDataStr);
            displayLoggedInView();
          }
        });
    
//toggle new article display
    $("#newArticle").click(function(evt) {
        $("#newArticleForm").toggleClass("noDisplay");
        console.log("toggle");
        evt.stopPropagation();
    });
    $("#closeNewArt").click(function(evt) {
        $("#newArticleForm").toggleClass("noDisplay");
        console.log("toggle");
        evt.stopPropagation();
    });
    
// process data from new article
  $("#submit").click(function(ev) {  
    var title= $( 'input[name=title]' ).val();
    var creator= "ionut";
    var content= $( 'input[name=content]' ).val();
    var image= "inform";
    ev.preventDefault();
    //prepare the form data to be sent to server
        var formData = new FormData();
        formData.append("title",title);
        formData.append("content",content);
        formData.append("image",image);
        
        var article = new Article({
            title:title,
            content:content,
            image:image
        });
        article.add(formData);
  });
  
// validate email  
  var emaillog=0;
  $( "input[name='user_email']" ).change(function() {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          var email = $("#user_email").val();
          emaillog = 0;
          if (re.test(email)) {
            $('input#user_email').removeClass("invalid").addClass("valid");
            emaillog++;
            return true;
          }
  	else{$('input#user_email').removeClass("valid").addClass("invalid");}
  });
// validate password
var passlog = 0;
  $( "input[name='user_password']" ).change(function() {
          var re = /^[a-zA-Z]\w{5,14}$/;
          var pass = $("#user_password").val();
          passlog = 0;
          if (re.test(pass)) {
            $('input#user_password').removeClass("invalid").addClass("valid");
            passlog++;
            return true;
          }
  	else{$('input#user_password').removeClass("valid").addClass("invalid");}
  });
  
  $("#login-btn").click(function(ev){
    if ((passlog==1)&&(emaillog==1)){
        ev.preventDefault();
        var userEmail = $('input[name:user_email]').val();
        var userPassword = $("input[name:user_password]").val();
        console.log("clic");
        var formData = new FormData();
        formData.append("email", userEmail);
        formData.append("password", userPassword);
        
        var user = new Users({
            user_email: userEmail ,
            user_password: userPassword
        });
        user.login(formData);
    }    
  });
  
});

