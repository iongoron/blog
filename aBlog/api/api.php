<?php


    $routes["articles"] = array(
                        "class"=>"Articles",
                        "method"=>"getAll");
                        
    $routes["articles/item"] = array(
                    "class"=>"Articles",
                    "method"=>"getItem");
    
    $routes["articles/add"] = array(
                    "class"=>"Articles",
                    "method"=>"addItem");
    
    $routes["articles/edit"] = array(
                    "class"=>"Articles",
                    "method"=>"editItem");
                    
    $routes["articles/delete"] = array(
                    "class"=>"Articles",
                    "method"=>"deleteItem");
                        
     $routes["users"] = array(
                            "class"=>"Users",
                            "method"=>"getUsers");
    $routes["users/session"] = array(
                            "class"=>"Login",
                            "method"=>"validate_request");
    $routes["users/signup"] = array(
                            "class"=>"Login",
                            "method"=>"createAccount");
    $routes["users/login"] = array(
                            "class"=>"Login",
                            "method"=>"Login");
    $routes["users/logout"] = array(
                            "class"=>"Login",
                            "method"=>"Logout");                        
    $routes["users/sessionU"] = array(
                            "class"=>"Login",
                            "method"=>"validateUserSession");   
    $routes["users/sessionA"] = array(
                            "class"=>"Login",
                            "method"=>"validateAdminSession");    
      
    define("API_DIR", "/aBlog/api/"); // current dir
    $redirectUrl = $_SERVER["REQUEST_URI"];
    $page = str_replace(API_DIR, "", $redirectUrl);
    $page = rtrim($page, "/");
    
    if (array_key_exists($page, $routes)) {
        $method = $_SERVER['REQUEST_METHOD'];
        switch ($method) {
            case "POST":
                // POST - add data
                $content = file_get_contents("php://input");
    //         echo "<pre>";
    // print_r($content);
    // exit;

                $data = json_decode($content, true);
        // echo ("here w is");

                if ($data) {
                    $_POST = $data;
                }
                break; 
            case "PUT":
                // PUT - update data
                $content = file_get_contents("php://input");
                $PUT = json_decode($content, true);
                break;
            case "DELETE":
                // DELETE - delete data

                $content = file_get_contents("php://input");
                $DELETE = json_decode($content, true);
                break;
        }

        
        require "controllers/".$routes[$page]["class"].".php";
        $controller = new $routes[$page]["class"]; 
        // echo ($controller);
        $result = $controller->$routes[$page]["method"]();
        
    }
    else {
        $result = ["error"=>"Page not found"];
        http_response_code(404);
    }
    
    header("Content-Type: application/json");
    echo json_encode($result);
    
   