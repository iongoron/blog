<?php
require "model/UsersModel.php";

 
    class Login {
        private $user;
        function __construct() {
            $this->user = new UsersModel();
        }
        
        function createAccount() {
        if (isset($_POST["email"])) {
            $errors = array();
        
        
            if (empty($_POST["email"]) || !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                $errors["email"] = "Email is invalid!";
            }
            if (empty($_POST["password"]) && strlen($_POST["password"] < 6)) {
                $errors["password"] = "Invalid password!";
            }
            if (empty($_POST["repassword"]) || $_POST["repassword"] != $_POST["password"]) {
                $errors["repassword"] = "Invalid repassword";
            }
            if (empty($errors)) {
                $salt = '$1$whatever$';
                $crypt_pswd = crypt($_POST["password"], $salt);
                $_POST["password"] = $crypt_pswd; 
                
               return $this->user->addUser($_POST);
            }
            else {
                return $errors;
            }
        } 
    }
        
        function Login() {

            if (isset($_POST["email"])) {
                $errors = array();

            if (empty($_POST["email"]) || empty($_POST["password"])) {
                    return $errors = "Empty credentials";
            }
             
            
            if (isset($_POST["email"]) && isset($_POST["password"])) {
                $salt = '$1$whatever$';
                $crypt_pass = crypt($_POST["password"], $salt);
                $_POST["password"] = $crypt_pass; 
                
                
                $loginUser  = $this->user->loginUser($_POST);
                
                if ($loginUser["password"] == $_POST["password"]){
                    $loggedIn = TRUE;
                } 
                
                if ($loggedIn == FALSE) {
                    return $errors = "Invalid credentials!";
                } else {
                    
                    $_SESSION['isLogged'] = TRUE; 
                        
                    if($loginUser["admin"] == 1) { 
                        $_SESSION['is_admin'] = TRUE;
                        $_SESSION['email'] = $_POST["email"];

                        return "Got yourself administrator rights!";
                    }
                    else{
                        $_SESSION['email'] = $_POST["email"];
                        return "You are logged in!";
                    }

                }
                
            }
            
        }
    }
        
        function logout() {
            unset( $_SESSION['isLogged']);
            unset($_SESSION['email']);
            session_destroy();
            
            return ["success"=>TRUE];
        }
        
        function validateUserSession () {
            if (!isset($_SESSION['isLogged']) && ($_SESSION['isLogged']) == false) {
                http_response_code(401);  
                return ["error"=>"UNAUTHORIZED"];       
            }else{
                return ["session"=>$_SESSION['isLogged'], "email" => $_SESSION['email']];       
        }   
    }
        
        function validateAdminSession() {
            if (!isset($_SESSION['is_admin']) && ($_SESSION['is_admin']) == false) {
                http_response_code(401);
                return ["error"=>"UNAUTHORIZED"];   
            } else {
                return ["session"=>$_SESSION['is_admin'], "email"=>$_SESSION['email']];
            }
        }
    }

    

