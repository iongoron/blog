<?php
require "models/ArticlesModel.php";

class Articles {
    private $articlesModel;
    
    function __construct() {
        $this->articlesModel = new ArticlesModel();
    }
    
    function getAll() {
        $search = (!empty($_GET["search"])) ? ($_GET["search"]) : "";
        $page = (!empty($_GET["page"])) ? $_GET["page"] : 1;
        $ipp = 5;                       
        $start = $page*$ipp-$ipp;
        $count = $this->articlesModel->countArticles($search);
        $nrPages = ceil($count/$ipp);
        $articles =['items'=> $this->articlesModel->selectArticles($search, $start, $ipp),
                     'nrPages' => $nrPages];  
        return $articles;
        
    }
    
    function getItem() {  
        $this->articlesModel->selectArticle($_GET['id']);
    }
    
    function addItem() {
        
        // var_dump($_POST);
        // GET FILES AND SAVE TO SERVER
        $tmpPath = $_FILES["file"]["tmp_name"];
        $filePath = "../uploads/".$_FILES["file"]["name"];
        move_uploaded_file($tmpPath, $filePath);
        // ADD FILE NAME INTO DB
        $_POST["image"] = $_FILES["file"]["name"];  
        // print_r($_POST);
        return $this->ArticlesModel->addArticle($_POST);          

    }
    
    function editItem() {
        // GET PUT VALUES FROM $PUT 
        global $PUT;
        echo $PUT["test"];
    }
    
    function deleteItem($item) {
        // $DELETE 
        global $DELETE;
        echo $DELETE["test"];
    }
}