 /*global localStorage */
 /*global $ */
 function User (options) {
        this.id= options.id;     
        this.email= options.email;
        this.password= options.password;
        this.name= options.name;
        this.age= options.age;
        this.gender= options.gender;
    }
    User.prototype.createAccount= function (formData) {
    
    // var that = this;
     $.ajax ({
        url: "https://roxana-roxanaalb.c9users.io/Blog-TemaFinala/api/users/createaccount",
        type: "POST",
        data: formData, 
        processData: false,
        contentType: false,
        success: function (resp) {
        
             console.log("Your account was created");
           
        },
        error: function (xhr, status, error) {
            alert ("Oops! Something went wrong");
        }
    });
}; 
User.prototype.login= function () {
     var dataLogin= {
     email: this.email,
     password: this.password
 } ;
//  var that = this;
    return $.ajax ({
        url: "https://roxana-roxanaalb.c9users.io/Blog-TemaFinala/api/users/login",
        type: "POST",
        data: dataLogin,
      
        success: function (resp) {
        
        },
       
        error: function (xhr, status, error) {
            // alert ("Oops! Something went wrong");
            
        }
    });
    
};

