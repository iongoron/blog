/* global $*/

function User(options){
    this.id = options.id;
    this.email = options.email;
    this.password = options.password;
    this.repassword = options.repassword;
}

User.prototype.createAccount=function(){
    var dataToSend ={
                    email:this.email,
                    password:this.password,
                    repassword:this.repassword
                    };

//  console.log(formData);
     return $.ajax({
        url: "https://exercise-ionutgoron.c9users.io/DoctorMaseluta/api/users/add",
        type: "POST",
        data: dataToSend,
        success: function(resp){
            console.log ("Account created");
        },
        error:function(xhr, status, error){
            alert("Something went wrong with your signup. Please try again!");
        }
    });
};
User.prototype.login=function (){
    
    
    var loginData = { 
                    email: this.email,
                    password: this.password
                    };
    return $.ajax ({
        url: "https://exercise-ionutgoron.c9users.io/DoctorMaseluta/api/users/login",
        type: "POST",
        data: loginData,
       
        success : function (resp) {
            console.log("You're logged in!");
        },
        error: function(xhr, status, error){
            alert ("Login failed");
        }
    });
};
// fac in folderul helpers un fisier pentru logout 


