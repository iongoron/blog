<?php

define("DB_HOST", 'localhost');
define("DB_NAME", 'CA_DB');
define("DB_USER", 'ionutgoron');
define("DB_PASS", '');

class DB {
    protected $dbh;
    protected $sth;
    
    function __construct() {
        try {
            $this->dbh = new PDO('mysql:host='.DB_HOST. ';dbname='.DB_NAME, DB_USER, DB_PASS);
            // echo "You have connected!";
        } catch (PDOExeption $e) {
            print "Error!: " . $e->getMessage(); 
        }
    }
    
    protected function selectAll($sql, $data=array()) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data);
        // var_dump($data); die;
        return $this->sth->fetchAll(PDO::FETCH_ASSOC);
    }
    
    protected function searchAll($sql, $data) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->exeute($data);
        return $this->sth->fetch(PDO::FETCH_ASSOC);
    }
    
    protected function countAll() {
        return $this->sth->rowCount();
    }
    
    protected function addItem($sql, $data) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data); 
        // var_dump($data); die;
        return $this->dbh->lastInsertId();
    }
    
    protected function selectItem ($sql, $data=array()) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data);
        return $this->sth->fetch(PDO::FETCH_ASSOC);
    }
    
    protected function updateItem($sql, $data) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data);
        return $this->sth->rowCount();   
    }
    
    protected function removeItem($sql, $data) {
        $this->sth = $this->dbh->prepare($sql);
        $this->sth->execute($data); 
        // var_dump($data);die;
        return $this->sth->rowCount();
        // return $sth->fetch(PDO::FETCH_ASSOC);
    }
}




