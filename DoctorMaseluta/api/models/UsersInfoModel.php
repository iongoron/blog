<?php
require_once "db.php";

class UsersInfoModel extends DB {
    
    function getAll($search = "", $start = 0, $limit = 20) {
        $sql = 'select * from users_info';
        $data = array();
        
        if ($search !== "") {
            $data = ['%' . $search . '%'];
            $sql .= ' where firstName like ?';
        }
        
        $sql .= ' limit ' . $start . ',' . $limit;
        
        return $this->selectAll($sql, $data);
    }
    
    function addUsersInfo($item) {
        $data = [$item['firstName'],
                 $item['lastName'],
                 $item['age'],
                 $item['adress'],
                 $item['file'],
                 $item['user_id']];
                 
        $sql = 'insert into users_info (firstName, lastName, age, adress, file, user_id) 
                values (?, ?, ?, ?, ?, ?)';
    
        return $this->addItem($sql, $data);
    }
    
    function selectUserDetails($user_id) {
        $data = [$item['user_id']];
        $sql = 'select users. *, users_info. * from users join users_info  
                on users_info.user_id = users.id where user_id =' . $user_id;
    // select users details from users_info and users, based on user_id
        return $this->selectItem($sql, $data);
    }
    function editUserDetails($item) {
        $data = [$item['firstName'],
                 $item['lastName'],
                 $item['age'],
                 $item['adress'],
                 $item['user_id']];
        $sql = 'update users_info set firstName = ?, lastName = ?, age = ?,
                adress = ? where user_id = ?';
        return $this->updateItem($sql, $data);
    }
    function deleteUserDetails($item) {
        $data= [$item['user_id']];
        $sql= 'delete from users_info where user_id = ?';
        return $this->removeItem($sql, $data);
    }
}
    