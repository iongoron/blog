<?php

require_once "db.php";

class UsersModel extends DB {
   
    function getAll($search = "", $start = 0, $limit = 20) {
        $sql = 'select * from users';
        // $data = array();
        
        if ($search !== "") {
            $data = ['%' . $search . '%'];
            $sql .= ' where email like ?';
        }
        
        $sql .= ' limit ' . $start . ',' . $limit;
        
        return $this->selectAll($sql, $data);
    }
    
    function addUser($item) {
        
        $data = [$item['email'],
                 $item['password']];
        
        $sql = 'insert into users (email, password) values (?, ?)';
        return $this->addItem($sql, $data);
    }
    function selectEmail($item) {
        $data= [$item['email']];
        $sql = 'select email from users where email = ?';
        return $this->selectItem($sql, $data);
    }
    function selectUser($item) {
        $data= [$item['email'],
                $item['password']];
        $sql = 'select * from users where (email = ? and password = ?)';
        return $this->selectItem($sql, $data);
    }
    function editUser($item) {
        $data = [$item ['password'],
                 $item ['id'] = $_SESSION['id']];
        $sql = 'update users set password =? where id = ?';
        return $this->updateItem($sql, $data);
    }
    function deleteUser($id) {
        $data= [$item['id'] = $_SESSION['id']];
        $sql= 'DELETE users. * ,users_info. * FROM users JOIN users_info WHERE 
                users_info.user_id = users.id AND users.id =' .$id;
        return $this->removeItem($sql, $data);
    }
    
    
}
