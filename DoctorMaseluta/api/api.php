<?php
session_start();
// echo "you are in api";

    // routes for tabel users
    $routes["users/add"] = array(
                              "class"=>"Users",
                              "method"=>"createAccount");
    $routes["users"] = array(
                              "class"=>"Users",
                              "method"=>"selectAll");
    $routes["users/login"] = array(
                              "class"=>"Users",
                              "method"=>"login"); 
    $routes["users/logout"] = array(
                              "class"=>"Users",
                              "method"=>"logout");  
                              
    $routes["users/session"] = array (
                              "class"=>"Users",
                              "method"=>"checkUserSession"); 
    $routes["users/sessionA"] = array (
                              "class"=>"Users",
                              "method"=>"checkAdminSession"); 
    $routes["users/edit"] = array (
                              "class"=>"Users",
                              "method"=>"editAccount");
    $routes["users/delete"] = array(
                              "class"=>"Users",
                              "method"=>"deleteAccount"); 
                              
    // routes for table users_info
    $routes["usersinfo"] = array(
                              "class"=>"Userinfo",
                              "method"=>"selectAll");
    $routes["usersinfo/add"] = array(
                              "class"=>"Userinfo",
                              "method"=>"addUsersInfo"); 
    $routes["usersinfo/item"] = array(
                              "class"=>"Userinfo",
                              "method"=>"selectUserDetails"); 
    
    $routes["usersinfo/edit"] = array(
                              "class"=>"Userinfo",
                              "method"=>"editUserDetails"); 
    $routes["usersinfo/delete"] = array(
                              "class"=>"Userinfo",
                              "method"=>"deleteUserDetails");
                             
                   
                             
                             
                              
        define ("API_DIR", "/DoctorMaseluta/api/");
        $redirectUrl= $_SERVER["REDIRECT_URL"];
        $page = str_replace(API_DIR, "", $redirectUrl);
        $page = rtrim($page, "/");
   
   
   
        if (array_key_exists($page, $routes)) {
   
            $method= $_SERVER['REQUEST_METHOD']; 
     
            switch ($method) {
   
                case "POST":
                    $content = file_get_contents("php://input");
                    $data= json_decode($content, true);
             
                    if ($data) {
              
                        $_POST= $data;
                     }
                     
                break;
    
                case "PUT":
    
                    $content = file_get_contents("php://input");
                    $PUT= json_decode($content, true);
            
                break;
                
                case "DELETE":
                    $content = file_get_contents("php://input");
                    $DELETE= json_decode($content, true);
             
                break;
            }
            
     
        require "controllers/" .$routes[$page]["class"].".php"; 
   
        $controller = new $routes[$page]["class"];
  
        $result = $controller->$routes[$page]["method"]();
        } else { 
            $result =["error"=>"Page not found!"];
    
            http_response_code(404);
        }

          

            header("Content-Type: application/json");
            echo json_encode($result);
