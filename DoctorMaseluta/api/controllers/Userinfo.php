<?php
require "models/UsersInfoModel.php";

class Userinfo {
    private $userInfo;
    
    function __construct(){
        $this->userInfo = new UsersInfoModel();
    }
    
    function selectAll() {
        return $this->userInfo->getAll();
    }
    
    function selectUserDetails(){
    // when we will use the session to get the user_id, we can modify here, $_GET instead of $_POST
        return $this->userInfo->selectUserDetails($_GET['user_id']);
        
    }
    
    function addUsersInfo(){
        $tmpPath = $_FILES["file"]["tmp_name"];
          
        $filePath= "../uploads/" . $_FILES["file"]["name"];
        move_uploaded_file($tmpPath, $filePath);
          
        $_POST["file"] = $_FILES["file"]["name"];
        // if you want to verify without session, comment if and else and introduce user_id
        if ($_POST['user_id'] = $SESSION['id']){
            return $this->userInfo->addUsersInfo($_POST);
        } else {
            return 'You must be logged in!';
        }
    }
    
    function editUserDetails(){
        global $PUT;
        // if you want to verify without session, comment if and else and introduce user_id
        // if it doesn't work with put and delete we can use user_id = $_SESSION['id']
        // in model instead of controller
        if ($PUT['user_id'] = $_SESSION['id']){
            return $this->userInfo->editUserDetails($PUT);
        } else {
            return 'You must be logged in!';
        }
    }
    
    function deleteUserDetails(){
        global $DELETE;
        if ($DELETE['user_id'] = $_SESSION['id']){
            return $this->userInfo->deleteUserDetails($DELETE);
        } else {
            return 'You must be logged in!';
        }  
    }
    
}