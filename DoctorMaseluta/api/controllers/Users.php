<?php
require "models/UsersModel.php";

class Users {
    private $userModel;
    
    function __construct() {
        $this->userModel = new UsersModel();
        
    }
    
    function selectAll() {
        return $this->userModel->getAll();
    }
    
    function createAccount() { 
        if (isset($_POST["email"])) {
            $errors= array();
            $email= $_POST["email"];
            $passwd= $_POST["password"];
            $repass= $_POST["repassword"];

            if (empty($email) || !filter_var($email,FILTER_VALIDATE_EMAIL)) {
                $errors["email"]= "Email invalid";
            
            }
        
            else {
                $checkEmail= $this->userModel->selectEmail($_POST);
                if ($checkEmail) {
                    $errors["email"] = "Email already taken!";
                }
            }
            if (empty($passwd) || empty($repass) || strlen($passwd)<6) {
                $errors["password"]= "Password invalid";
            }
            else if ($passwd != $repass) {
                $errors["password"]= "Password and repassword not equal!";
            }
            if (empty($errors)) {
                $salt= "$1$maseluta$";
                $password = crypt($passwd, $salt);
        
                $_POST["password"]= $password;
                return $this->userModel->addUser($_POST);
            } 
            else {
                return $errors;
            }
        }
    }
    
    function login() {
        // var_dump ($_POST);
        $email= $_POST['email'];
        $passwd= $_POST['password'];
        

        if (isset($email) && isset($passwd)) {
            $salt= "$1$maseluta$";
            $password= crypt($passwd, $salt);
            $_POST["password"]= $password;
            $user = $this->userModel->selectUser($_POST);
            
            if ($user !== FALSE) {
                $_SESSION['isLogged'] = TRUE;
                $_SESSION['email']= $email;
                $_SESSION['id']= $user['id'];
                
                if ($user["user_value"] == 1){
                    $_SESSION['admin'] = TRUE;
                    return 'Administrative rights';
                } 
                else {
                    return "You logged in!";
                }
            } 
            
            else {
                return $errors= "Invalid credentials";
            }
        } 
    }
    
    function editAccount() {
        global $PUT;
        $errors= array();
    
        $passwd=$PUT["password"];
        $repass=$PUT["repassword"];
   
        if (empty($passwd)|| empty($repass) || strlen($passwd)<6) {
            $errors["password"]= "Password invalid";
        }
        else if ($passwd != $repass) {
            $errors["password"]= "Password and repassword not equal!";
        }
        if (isset($passwd)) {
            $salt= "$1$maseluta$";
            $password= crypt($passwd, $salt);
            $PUT["password"]= $password;
            return $this->userModel->editUser($PUT);
        }
        else {
        
         return $errors;
        }
    }
    
    function deleteAccount() {
        global $DELETE;
        return $this->userModel->deleteUser($DELETE);
    }
    function logout() {
         unset($_SESSION["isLogged"]);
         unset($_SESSION["email"]);
         unset($_SESSION["id"]);
         unset($_SESSION["admin"]);
         session_destroy();
         
        return ["success"=>TRUE];
    }   
    
    function checkUserSession() {
    
        if (isset($_SESSION["isLogged"])) {
      
            return ["isLogged"=>TRUE, "email" => $_SESSION["email"]]; 
           
            
        } 
        else {
          
        http_response_code(401);  
        return ["error"=>"UNAUTHORIZED"];
    
  
        }
    }
    
    function checkAdminSession() {
        if (!isset($_SESSION['admin']) ) {
            http_response_code(401);
            return ["error"=>"UNAUTHORIZED"];   
        } else {
            return ["session"=>$_SESSION['admin'], "email"=>$_SESSION['email']];
            }
    }
    
}


